//
//  FormatDisplay.swift
//  runkeeper_clone
//
//  Created by Admin on 2018-07-04.
//  Copyright © 2018 Julijus Ergül. All rights reserved.
//

import UIKit

class FormatDisplay: NSObject {
    
    static func distance(_ distance: Double) -> String {
        let distanceMeasurement = Measurement(value: distance, unit: UnitLength.meters)
        return FormatDisplay.distance(distanceMeasurement)
    }
    
    static func distance(_ distance:Measurement<UnitLength>)->String{
        let formatter = MeasurementFormatter()
        return formatter.string(from: distance)
    }
    
    static func time( seconds: Int) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .positional
        formatter.zeroFormattingBehavior = .pad
        return formatter.string(from: TimeInterval(seconds))!
    }
}
