//
//  LocationManager.swift
//  runkeeper_clone
//
//  Created by Admin on 2018-07-02.
//  Copyright © 2018 Julijus Ergül. All rights reserved.
//

import CoreLocation

class LocationManager {
    static let shared = CLLocationManager()
    
    private init() { }
    
}
